# TempleOS font

TempleOS font with additional characters and some tweaks.

Made with [Bits n' Picas](https://github.com/kreativekorp/bitsnpicas)

Originally from [rendello/templeos_font](https://github.com/rendello/templeos_font)

